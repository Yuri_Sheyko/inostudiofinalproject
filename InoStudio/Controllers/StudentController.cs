﻿using InoStudio.Models;
using InoStudio.Services;
using InoStudio.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace InoStudio.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        private readonly StudentService _students = new StudentService(new Repositoies.StudentRepository());

        public StudentController()
        {
        }
        public async Task<ActionResult> Index()
        {

            var students = await _students.GetAllStudentsAsync();
            if(students==null)
            {
                students = new List<StudentModel>() { new StudentModel { LastName = "Shcherbakov", FirstName = "Maxim", Patronymic = "Igorevich", ID = 1, Marks = new MarksModel() { marks = new Dictionary<DateTime, int>() } } };
                students.Last().Marks.marks.Add(DateTime.Now, 5);
            }
            return View(students);//(new List<StudentModel> { students });
        }
        public ActionResult AddStudent()
        {
            var model = new StudentViewModel()
            {
                Title = "Add new student",
                ButtonTitle = "Confirm",
                RedirectURL = Url.Action("Index", "Student")
            };
            return View(model);
        }

        public async Task<ActionResult> Deteils(int id)
        {
            var student = await _students.GetStudentByIdAsync(id);
            return View(new StudentViewModel { ID = student.ID, StudentName = student.LastName });
        }
        [HttpPost]
        public async Task<ActionResult> ApplyEdit(StudentViewModel model, string url)
        {
            if (!ModelState.IsValid)
                return View(model);
            var student = await _students.GetStudentByIdAsync(model.ID);
            if(student!=null)
            {
                student.LastName = model.StudentName;
                student.FirstName = model.StudentFirstName;
                student.Patronymic = model.StudentPatronymic;
                await _students.ModifyStudentAsync(student);
            }
            return await Index();
        }
        public async Task<ActionResult> Modify(int id)
        {
            var student = await _students.GetStudentByIdAsync(id);
            var newModel = new StudentViewModel()
            {
                StudentName = student.LastName,
                StudentFirstName = student.FirstName,
                StudentPatronymic = student.Patronymic,
                Title = "Modify student",
                ButtonTitle = "Modify",
                RedirectURL = Url.Action("Index", "StudentController"),
                ID = student.ID
            };
            return View(newModel);
        }
        public async Task<ActionResult> Delete(int id)
        {
            await _students.DeleteStudentByIdAsync(id);
            return RedirectToAction("Index", "StudentController");
        }
        public async Task<ActionResult> ApplyAdd(StudentViewModel model,string url)
        {
            if (!ModelState.IsValid)
                return View(model);
            var student = new StudentModel()
            {
                LastName = model.StudentName,
                FirstName = model.StudentFirstName,
                Patronymic = model.StudentPatronymic,
                Marks = new MarksModel() {marks = new Dictionary<DateTime, int>()}
            };
            await _students.AddStudentAsync(student);
            return RedirectToAction("Index","Student");
        }

    }
}