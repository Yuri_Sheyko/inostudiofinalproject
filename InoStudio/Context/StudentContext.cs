﻿
using System.Data.Entity;
using InoStudio.Models;

namespace InoStudio.Context
{
    public class StudentContext : DbContext
    {
        public StudentContext() : base("DefoultConnection")
        {

        }

        public DbSet<StudentModel> Students { get; set; }
    }
}