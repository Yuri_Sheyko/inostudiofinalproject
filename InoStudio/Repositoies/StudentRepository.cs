﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using InoStudio.Context;
using InoStudio.Models;

namespace InoStudio.Repositoies
{
    public class StudentRepository : IStudentRepository
    {
        public StudentRepository()
        { 
        }
        public async Task<StudentModel> AddStudent(StudentModel subj)
        {
            StudentModel res = null;
            using (var context = new StudentContext())
            {
                res = context.Students.Add(subj);
                await context.SaveChangesAsync();
            }
            return res;
        }

        public async Task DeleteStudentById(int id)
        {
            using (var context = new StudentContext())
            {
                var obj = await context.Students.FirstOrDefaultAsync(x => x.ID == id);
                context.Entry(obj).State = EntityState.Deleted;
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<StudentModel>> GetAllStudents()
        {
            List<StudentModel> res = null;
            using (var context = new StudentContext())
            {
                res = await context.Students.ToListAsync();
            }
            return res;
        }

        public async Task<StudentModel> GetStudentById(int id)
        {
            StudentModel res = null;

            using (var context = new StudentContext())
            {

                res = await context.Students.FirstOrDefaultAsync(x => x.ID == id);
            }
            return res;
        }

        public async Task<StudentModel> ModifyStudent(StudentModel subj)
        {
            using (var context = new StudentContext())
            {
                context.Entry(subj).State = EntityState.Modified;
                await context.SaveChangesAsync();
            }
            return subj;
        }
    }
}