﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InoStudio.ViewModels
{
    public class StudentViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string ButtonTitle { get; set; }
        public string RedirectURL { get; set; }

        [Display(Name = "Name of students")]
        public string StudentName { get; set; }
        public string StudentFirstName { get; set; }
        public string StudentPatronymic { get; set; }

        public Dictionary<DateTime, int> StudentMark { get; set; }
    }
}