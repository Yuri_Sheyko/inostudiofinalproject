﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InoStudio.Models
{
    public class MarksModel
    {
        public int ID { get; set; }
        public Dictionary<DateTime, int> marks { get; set; }
    }
}