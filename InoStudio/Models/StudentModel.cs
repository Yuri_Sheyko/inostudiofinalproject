﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InoStudio.Models
{
    public class StudentModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public double AveregeMark { get; set; }
        public int Skpips { get; set; }
        public MarksModel Marks { get; set; }
    }
}